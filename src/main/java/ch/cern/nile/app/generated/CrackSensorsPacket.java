// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package ch.cern.nile.app.generated;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStruct;
import io.kaitai.struct.KaitaiStream;
import java.io.IOException;
import java.util.Arrays;


/**
 * @see <a href="https://cernbox.cern.ch/s/JGcyFAzgRrdltWV">Source</a>
 */
public class CrackSensorsPacket extends KaitaiStruct {
    public static CrackSensorsPacket fromFile(String fileName) throws IOException {
        return new CrackSensorsPacket(new ByteBufferKaitaiStream(fileName));
    }

    public CrackSensorsPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public CrackSensorsPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public CrackSensorsPacket(KaitaiStream _io, KaitaiStruct _parent, CrackSensorsPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        this.magic = this._io.readBytes(1);
        if (!(Arrays.equals(magic(), new byte[] { 0 }))) {
            throw new KaitaiStream.ValidationNotEqualError(new byte[] { 0 }, magic(), _io(), "/seq/0");
        }
        this.temperatureRawHidden = this._io.readS2be();
        this.batteryVoltageRawHidden = this._io.readU1();
        this.displacementRaw = this._io.readF4be();
        this.displacementPhysical = this._io.readF4be();
    }
    private Double temperature;

    /**
     * Temperature reading in degrees Celsius.
     */
    public Double temperature() {
        if (this.temperature != null)
            return this.temperature;
        double _tmp = (double) (((temperatureRawHidden() * 0.01) + 0));
        this.temperature = _tmp;
        return this.temperature;
    }
    private Double batteryVoltage;

    /**
     * Battery voltage in mV.
     */
    public Double batteryVoltage() {
        if (this.batteryVoltage != null)
            return this.batteryVoltage;
        double _tmp = (double) (((batteryVoltageRawHidden() * 4.71) + 2500));
        this.batteryVoltage = _tmp;
        return this.batteryVoltage;
    }
    private byte[] magic;
    private short temperatureRawHidden;
    private int batteryVoltageRawHidden;
    private float displacementRaw;
    private float displacementPhysical;
    private CrackSensorsPacket _root;
    private KaitaiStruct _parent;

    /**
     * Frame magic byte
     */
    public byte[] magic() { return magic; }

    /**
     * Raw temperature reading from the sensor. The actual value can be calculated as temperature_raw * 0.01.
     */
    public short temperatureRawHidden() { return temperatureRawHidden; }

    /**
     * Raw battery voltage reading from the sensor. The actual value can be calculated as battery_voltage_raw * 4.71 + 2500.
     */
    public int batteryVoltageRawHidden() { return batteryVoltageRawHidden; }

    /**
     * Displacement raw value  in mV/V.
     */
    public float displacementRaw() { return displacementRaw; }

    /**
     * Displacement physical value in mm.
     */
    public float displacementPhysical() { return displacementPhysical; }
    public CrackSensorsPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
}
