package ch.cern.nile.app.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

public class CrackSensorsPacketTest {

    private static final JsonElement DATA_FRAME = TestUtils.getDataAsJsonElement("AAc/3sLha7FAX8b9");

    @Test
    void givenDataFrame_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(DATA_FRAME, CrackSensorsPacket.class);

        assertEquals(18.55, packet.get("temperature"));
        assertEquals(3545.62, packet.get("batteryVoltage"));
        assertEquals((float) -112.710335, packet.get("displacementRaw"));
        assertEquals((float) 3.4965203, packet.get("displacementPhysical"));
    }

}
